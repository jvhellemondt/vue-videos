Minimal (bootstrap) front-end UI to search Youtube videos by providing a search bar.

Youtube video's searched appear as list, in which the results are clickable, which loads and shows the respective embedded video.


# video-browser

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
